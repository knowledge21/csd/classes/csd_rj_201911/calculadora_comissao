import math

LIMITE_COMISSAO = 10000
COMISSAO_MENOR_10K = 0.05
COMISSAO_MAIOR_10K = 0.06


def calcular(valor_venda):
    if valor_venda <= LIMITE_COMISSAO:
        comissao = valor_venda * COMISSAO_MENOR_10K
    else:
        comissao = valor_venda * COMISSAO_MAIOR_10K
    return truncar_em_duas_casas(comissao)


def truncar_em_duas_casas(valor):
    return math.floor(valor * 100) / 100
